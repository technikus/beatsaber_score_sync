#!/usr/bin/env python3
import pip
import os
import requests
import json
import time
try:
    import progressbar
except ImportError:
    pip.main(['install', "progressbar2"]) 
    import progressbar


def getResponse(url_string : str) -> requests.Response:
    # respecting scoresabers api limits
    getResponse.count += 1
    current_time_delta = time.time() - getResponse.last_call
    if (current_time_delta) < getResponse.api_delta:
        #print("sleeping for %.3f seconds" % (getResponse.api_delta - current_time_delta))
        #print("*", end="")
        time.sleep(getResponse.api_delta - current_time_delta)
    getResponse.last_call = time.time()
    return requests.get(url_string)
getResponse.last_call = 0
getResponse.api_delta = 0.15
getResponse.count = 0
    
def getFullUserInfo(player_id : str):
    response = getResponse("https://scoresaber.com/api/player/%s/full" % player_id)
    if response.status_code == 200:
        return json.loads(response.text)
    return None

def getRecentUserScores(player_id : str, page : int):
    response = getResponse("https://scoresaber.com/api/player/%s/scores?limit=100&sort=recent&page=%d" % (player_id, page))
    if response.status_code == 200:
        return json.loads(response.text)
    return None

def getScoresForUser(username:str, difficulty:int, songhash:str):
    response = getResponse("https://scoresaber.com/api/leaderboard/by-hash/%s/scores?difficulty=%d&search=%s" % (songhash, difficulty, username))
    if response.status_code == 200:
        return json.loads(response.text)
    return None

def getLeaderboard(difficulty:int, songhash:str, country_code=None):
    if country_code is None:
        response = getResponse("https://scoresaber.com/api/leaderboard/by-hash/%s/scores?difficulty=%d" % (songhash, difficulty))
    else:
        response = getResponse("https://scoresaber.com/api/leaderboard/by-hash/%s/scores?difficulty=%d&countries=%s" % (songhash, difficulty, country_code))
    if response.status_code == 200:
        return json.loads(response.text)
    return None

def timestringToTimestamp(timestr:str) -> int:
    return time.mktime(time.strptime(timestr, "%Y-%m-%dT%H:%M:%S.000Z"))
    
def fullCombo(combo : bool) -> str:
    if (combo):
        return "FC"
    return "  "
def printScore(score) -> None:
    print("%s:\t%8d %s (%d)" % (score["leaderboardPlayerInfo"]["name"].ljust(20), score["modifiedScore"], fullCombo(score["fullCombo"]), timestringToTimestamp(score["timeSet"])))
    
def difficultyString(difficulty : int) -> str:
    if difficulty == 1:
        return "Easy   "
    elif difficulty == 3:
        return "Normal "
    elif difficulty == 5:
        return "Hard   "
    elif difficulty == 7:
        return "Expert "
    elif difficulty == 9:
        return "Expert+"
    else:
        return "xxxxxxx"
    
def getUsersFromLeaderboard(scores):
    users = []
    for score in scores:
        users.append(score["leaderboardPlayerInfo"]["name"])
    return users

def updateUsernameSuffixes(usernames, username_suffixes, leaders, suffix):
    for i in range(len(usernames)):
        if usernames[i] in leaders:
            username_suffixes[i] += suffix + str(leaders.index(usernames[i])+1)
    return username_suffixes

class ScoreEntry:
    def __init__(self, username):
        self.username = username
        self.score = 0
        self.suffix = ""
        self.full_combo = False
        self.timestamp = 0
        self.difficulty = 0
        self.song = "XX"
        self.is_valid = False
        
    def fromScoreEntry(self, score_entry):
        self.username = score_entry["leaderboardPlayerInfo"]["name"]
        self.score = score_entry["modifiedScore"]
        self.full_combo = score_entry["fullCombo"]
        self.timestamp = timestringToTimestamp(score_entry["timeSet"])
        self.is_valid = True
        
    def updateSuffix(self, leaders, suffix) -> int:
        if self.username in leaders:
            index = leaders.index(self.username)
            if len(self.suffix) > 1:
                self.suffix += " "
            self.suffix += suffix + str(index+1)
            return index
        return -1
        
    def setFromArchive(self, archive_dict, difficulty, song_hash):
        self.score = archive_dict["score"]
        self.suffix = "W%d" % archive_dict["rank"]
        self.full_combo = archive_dict["full_combo"]
        self.timestamp = archive_dict["timestamp"]
        self.difficulty = int(difficulty)
        self.song = song_hash
        self.is_valid = True
    
    def fromLeaderboard(self, leaderboard, suffix):
        leaders = getUsersFromLeaderboard(leaderboard["scores"])
        index = self.updateSuffix(leaders, suffix)
        if index >= 0:
            self.fromScoreEntry(leaderboard["scores"][index])
    
    def isValid(self):
        return self.is_valid
    
    def invalidString(self):
        if not self.is_valid:
            return "INVALID"
        return ""
    
    def prettyString(self):
        return "%s:\t%8d %s (%d) %s" % (self.getModifiedNameString().ljust(20), self.score, fullCombo(self.full_combo), self.timestamp, self.invalidString())
    
    def prettyPrint(self, skip_invalid = True):
        if skip_invalid and not self.is_valid:
            return
        print(self.prettyString())
        
    def getModifiedNameString(self) -> str:
        if len(self.suffix) > 1:
            return "~%s [%s]~" % (self.username, self.suffix)
        return "~%s~" % self.username
        
    def getLocalLeaderboardEntry(self):
        return {"_score": self.score,"_playerName": self.getModifiedNameString(),"_fullCombo":self.full_combo,"_timestamp": int(round(self.timestamp))}
    
class localLeaderboardID:
    def __init__(self, full_id : str):
        try:
            self.full_id = full_id
            self.is_valid = True
            self.difficulty_str = full_id[53:]
            self.difficulty = self.getDifficultyIntFromStr(self.difficulty_str)
            self.song_hash = full_id[13:53]
        except:
            self.is_valid = False
        
    def getDifficultyIntFromStr(self, string):
        if string == "Easy":
            return 1
        elif string == "Normal":
            return 3
        elif string == "Hard":
            return 5
        elif string == "Expert":
            return 7
        elif string == "ExpertPlus":
            return 9
        else:
            self.is_valid = False
            
    def prettyString(self):
        if not self.is_valid:
            return "INVALID %s INVALID" % self.full_id
        return "Hash: %s\nDifficulty: %s (%d)" % (self.song_hash, self.difficulty_str, self.difficulty)
    
    def prettyPrint(self):
        if not self.is_valid:
            return
        print(self.prettyString())
        
def getScoreSaberEntries(song_hash, difficulty, country_codes, num_world_leaders, num_country_leaders):
    
    username_score_entries = getEntriesFromArchive(song_hash, difficulty)
    
    world_leaders = []
    if num_world_leaders > 0:
        world_leaderboard = getLeaderboard(difficulty, song_hash)
        if world_leaderboard is None:
            #print("  skipping")
            return []
        for i in range(num_world_leaders):
            try:
                world_leader = ScoreEntry(world_leaderboard["scores"][i]["leaderboardPlayerInfo"]["name"])
                world_leader.fromLeaderboard(world_leaderboard, "W")
                world_leaders.append(world_leader)
            except:
                pass
    
    # get country leaderboard:
    country_leaders = []
    if num_country_leaders > 0:
        for country_code in country_codes:
            country_leaderboard = getLeaderboard(difficulty, song_hash, country_code=country_code)
            if not country_leaderboard is None:
                for i in range(num_country_leaders):
                    try:
                        country_leader = ScoreEntry(country_leaderboard["scores"][i]["leaderboardPlayerInfo"]["name"])
                        country_leader.fromLeaderboard(country_leaderboard, country_code)
                        country_leaders.append(country_leader)
                    except:
                        pass
                
                for entry in username_score_entries:
                    entry.fromLeaderboard(country_leaderboard, country_code)
                for entry in world_leaders:
                    entry.fromLeaderboard(country_leaderboard, country_code)
            
    # fill user scores if not filled already
    valid_user_score_entries = []
    for entry in username_score_entries:
        if not entry.isValid():
            print("HOW DID THIS HAPPEN?")
            response = getScoresForUser(entry.username, difficulty, song_hash)
            if not response is None:
                entry.fromScoreEntry(response["scores"][0])
        if entry.isValid():
            valid_user_score_entries.append(entry)
            
    entries_to_add = valid_user_score_entries
    for potential_entry in world_leaders+country_leaders:
        score_usernames = [entry_gen.username for entry_gen in entries_to_add]
        if not potential_entry.username in score_usernames:
            entries_to_add.append(potential_entry)
            
            
    # if not world_leader.username in user_names:
    #     entries_to_add.append(world_leader)
    # if not country_leaderboard is None and not country_leader.username in user_names and world_leader.username != country_leader.username:
    #     entries_to_add.append(country_leader)
    # entries_to_add += valid_user_score_entries
    # print("")
    # for entry in entries_to_add:
    #     entry.prettyPrint()
    return entries_to_add

class PlayerInfo:
    def __init__(self, full_player_info):
        self.player_id = str(full_player_info["id"])
        self.username = full_player_info["name"]
        self.total_scores = full_player_info["scoreStats"]["totalPlayCount"]
        self.rank = full_player_info["rank"]
        self.country_rank = full_player_info["countryRank"]
    
    def print(self):
        print("%s (%d, #%d)"% (self.username, self.total_scores, self.rank))
        
        
class PlayerScoreEntry:
    def __init__(self, score = None):
        if not score is None:        
            self.rank = score["score"]["rank"]
            self.score = score["score"]["modifiedScore"]
            self.timestamp = int(timestringToTimestamp(score["score"]["timeSet"]))
            self.full_combo = score["score"]["fullCombo"]
        
    def print(self):
        print("#%d (%d) %d %r" % (self.rank, self.score, self.timestamp, self.full_combo))
        
    def toDict(self):
        return {"rank": self.rank, "score": self.score ,"timestamp": self.timestamp, "full_combo": self.full_combo}

class SongScoreEntry:
    def __init__(self, score):
        self.song_hash = score["leaderboard"]["songHash"]
        self.song_name = score["leaderboard"]["songName"]
        self.song_artist = score["leaderboard"]["songAuthorName"]
        self.level_author = score["leaderboard"]["levelAuthorName"]
        self.difficulty = score["leaderboard"]["difficulty"]["difficulty"]
    def print(self):
        print("%s\n%s by %s (%s)" % (self.song_hash, self.song_name, self.song_artist, self.level_author))
        
    def toDict(self):
        return {"name": self.song_name, "artist": self.song_artist, "level_author": self.level_author}
    
def updatePlayerScores(player_ids, read_only = False):
    global score_archive
    score_archive_filename = "player_scores_archive.json"
    try:
        with open(score_archive_filename,'r') as score_archive_file:
            score_archive = json.load(score_archive_file)
        if read_only:
            return
    except:
        print("could not read archive file, starting fresh.")
        score_archive = {}
    
    for player_id in player_ids:
        
        # get player info
        result = getFullUserInfo(player_id)
        if result is None:
            print("could not resolve player_id: %s" % str(player_id))
            continue
        p = PlayerInfo(result)
        p.print()
        
        try:
            score_archive[player_id]["name"]
        except:
            print("creating new entry.")
            score_archive[player_id] = {"name": p.username, "scores": {}}
        
        player_scores = score_archive[player_id]["scores"]
        
        get_scores = True
        page_count = 1
        while get_scores:
            result = getRecentUserScores(p.player_id, page_count)
            for score in result["playerScores"]:
                song = SongScoreEntry(score)
                player_score = PlayerScoreEntry(score)
                try:
                    # check if a score with this timestamp already exists
                    existing_timestamps = [score["timestamp"] for score in player_scores[song.song_hash][str(song.difficulty)]]
                    
                    if not (player_score.timestamp in existing_timestamps):
                        player_scores[song.song_hash][song.difficulty].append(player_score)
                    else:
                        index = existing_timestamps.index(player_score.timestamp)
                        player_scores[song.song_hash][str(song.difficulty)][index]["rank"] = player_score.rank
                except:
                    try: 
                        player_scores[song.song_hash][str(song.difficulty)] = [player_score.toDict()]
                    except:
                        player_scores[song.song_hash] = {"song": song.toDict()}
                        player_scores[song.song_hash][str(song.difficulty)] = [player_score.toDict()]
            if page_count * result["metadata"]["itemsPerPage"] > result["metadata"]["total"]:
                get_scores = False
            page_count += 1
        score_archive[player_id]["scores"] = player_scores
    with open(score_archive_filename, 'w') as outfile:
      json.dump(score_archive, outfile)

def getEntriesFromArchive(song_hash, difficulty):
    try:
        score_archive.keys()
    except:
        print("archive not loaded yet.")
        updatePlayerScores([], read_only=True)
    results = []
    user_ids = score_archive.keys()
    for user_id in user_ids:
        username = score_archive[user_id]["name"]
        try:
            #print(score_archive[user_id]["scores"][song_hash]["song"]["name"])
            for score in score_archive[user_id]["scores"][song_hash][str(difficulty)]:
                entry = ScoreEntry(username)
                entry.setFromArchive(score, difficulty, song_hash)
                results.append(entry)
        except:
            pass
    return results

def getSongFromArchive(song_hash):
    try:
        score_archive.keys()
    except:
        print("archive not loaded yet.")
        updatePlayerScores([], read_only=True)
    user_ids = score_archive.keys()
    for user_id in user_ids:
        try:
            score_archive[user_id]["scores"][song_hash]["song"]["name"]
            return score_archive[user_id]["scores"][song_hash]["song"]
        except:
            pass
    return {"name": "unknown", "artist": "unknown", "level_author": "unknown"}

def writeLocalLeaderboardPretty(filename, local_leaderboard_data):
    with open(filename, "w", encoding="utf-8") as pretty_file:
        for leaderboard in local_leaderboard_data["_leaderboardsData"]:
            leaderboard_entry = localLeaderboardID(leaderboard["_leaderboardId"])
            #leaderboard_entry.prettyPrint()
            if leaderboard_entry.is_valid:
                pretty_file.write("\n\n%s\n" % leaderboard_entry.song_hash)
                song = getSongFromArchive(leaderboard_entry.song_hash)
                pretty_file.write("  %s by %s (%s) [%s]\n" % (song["name"], song["artist"], song["level_author"], leaderboard_entry.difficulty_str))
                
                for i in range(len(leaderboard["_scores"])):
                    pretty_file.write("%s\t| %8d %s\n" %(leaderboard["_scores"][i]["_playerName"].ljust(25), leaderboard["_scores"][i]["_score"], fullCombo(leaderboard["_scores"][i]["_fullCombo"])))


     
    
local_leaderboard_path = str(os.getenv('LOCALAPPDATA'))+"Low\\Hyperbolic Magnetism\\Beat Saber\\LocalLeaderboards.dat"

#output_leaderboard_path = "modified.dat"

try:
    with open("config.json",'r') as config_file:
      config = json.load(config_file)
except Exception as e:
    print(e)
    print("could not read config file, creating default file.")
    with open("config.json",'w') as config_file:
        config_file.write("{\n  \"local_leaderboard_path\": \"\",\n  \"country_codes\": [],\n  \"num_world_leaders\": 2,\n  \"num_country_leaders\": 2,\n  \"player_ids\": []\n}")
try:
    num_world_leaders = config["num_world_leaders"]
except:
    print("could not read num_world_leaders from config, using default = 2")
    num_world_leaders = 2
try:
    num_country_leaders = config["num_country_leaders"]
except:
    print("could not read num_country_leaders from config, using default = 2")
    num_country_leaders = 2
try:
    player_ids = config["player_ids"]
except:
    print("could not read usernames from config, using default = []")
    usernames = []
try:
    country_codes = config["country_codes"]
except:
    print("could not read country_codes from config, using default = []")
    country_codes = []
try:
    if len(config["local_leaderboard_path"]) > 2:
        with open(config["local_leaderboard_path"],'r') as json_file_leaderboard:
            local_leaderboard_path =  config["local_leaderboard_path"]
    else:
        raise Exception
except:
    print("could not read local_leaderboard_path, using default of ", local_leaderboard_path)

time.sleep(0.5)
#sys.exit()

with open(local_leaderboard_path,'r') as json_file_leaderboard:
  local_leaderboard_data = json.load(json_file_leaderboard)

print("Updating user score archive..")
updatePlayerScores(player_ids)

print("Updating World and Country leaders..")
time.sleep(0.5)
bar = progressbar.ProgressBar()
for leaderboard in bar(local_leaderboard_data["_leaderboardsData"]):
    leaderboard_entry = localLeaderboardID(leaderboard["_leaderboardId"])
    #leaderboard_entry.prettyPrint()
    if leaderboard_entry.is_valid:
        # clean entries starting and ending with '~'
        keep_entries = []
        for i in range(len(leaderboard["_scores"])):
            if leaderboard["_scores"][i]["_playerName"][0] != '~' and leaderboard["_scores"][i]["_playerName"][-1] != '~':
                keep_entries.append(leaderboard["_scores"][i])
        leaderboard["_scores"] = keep_entries
        
        entries_to_add = getScoreSaberEntries(leaderboard_entry.song_hash, leaderboard_entry.difficulty, country_codes, num_world_leaders, num_country_leaders)
        for entry in entries_to_add:
            leaderboard["_scores"].append(entry.getLocalLeaderboardEntry())
            
        leaderboard["_scores"] = sorted(leaderboard["_scores"], key=lambda k: k['_score'], reverse=True)

print("Needed %d api accesses for %d entries" % (getResponse.count, len(local_leaderboard_data["_leaderboardsData"])))

with open(local_leaderboard_path, 'w') as outfile:
  json.dump(local_leaderboard_data, outfile, separators=(',', ':'))
  
writeLocalLeaderboardPretty("pretty.txt", local_leaderboard_data)
