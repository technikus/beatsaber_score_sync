# beatsaber_score_sync
Reads settings from [scoresaber.com](https://scoresaber.com/) and adds them to the local leaderboard (seen in party mode).
`local_leaderboard_path` should point to the location of the LocalLeaderboards file. If empty, the default location of 

`%userprofile%\AppData\LocalLow\Hyperbolic Magnetism\Beat Saber\LocalLeaderboards.dat`

This script will add the `num_world_leaders` best worldwide and `num_country_leaders` best country scores. It is possible to select multiple countries.

Additionally, the best score of each user with given scoresaber `player_ids` is added, multiple player_ids can be specified. 

User scores are rather fast, but world an country leaders need one API access per song and difficulty. Therefore, world leaders and two countries would need 3 accesses. In order to respect the API request limits of scoresaber of 400 requests per minute, subsequent API calls are delayed to only send a request ever 0.15 seconds. World and country scores are marked with an additional tag, full combo scores are indicated with `FC`:
```
  Sail by Devildriver (Squishyboob) [Hard]
~Timini105 [W1]~            |   591644   
~technikus [W2 AT1]~        |   586564 FC
local score entry           |   569249
~Simi_4_AUT [AT2]~          |   529968   
~Duality [AT3]~             |   526006   
another local score entry   |   427546   
```
Updating for 300 songs takes around 2 minutes.

Entries previously added by this script are removed, updating the score entries everytime. This can be easily done since all added entries are wrapped in `~` to identify them. Every entry starting and ending in `~` is removed from the scoreboard in the beginning. 

After updating the LocalLeaderboard, the file `pretty.txt` is generated, providing the updated leaderboards in ACII table form. (see above)
The username scores are stored locally in `player_scores_archive.json`, allowing to store the evolution of players (not yet implemented).

```json
{
  "local_leaderboard_path": "path-to-file/LocalLeaderboards.dat",
  "country_codes": ["US"],
  "num_world_leaders": 2,
  "num_country_leaders": 2,
  "player_ids": ["12345678901234567"]
}
```
If no config is provided, a default config file is generated and used.

## Installation
Python is required to run.
This script relies on `progressbar2`, it will attempt to install it with pip if it is not yet available.
